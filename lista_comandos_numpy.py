%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt


!ls


fname = 'vxi.txt'



vxi = np.genfromtxt(fname, names=True, delimiter=',', unpack=True)



vxi



plt.scatter(vxi['I'], vxi['V'])



Obter a resistência do resistor a partir dos dados.

x = np.array(vxi['I'])
x = np.row_stack(x)
x



y = np.array(vxi['V'])
y



# http://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.lstsq.html
res = np.linalg.lstsq(x, y)[0]
print "Coeficiente:", res




# definição da equação
def i(r, v):
    return v/r

v1 = np.arange(0.0, 160.0, 0.1)
plt.plot(i(r = res, v = v1), v1, 'k')
plt.scatter(vxi['I'], vxi['V'])
plt.title("Curva VxI")
plt.xlabel("I (mA)")
plt.ylabel("V (V)")



